
#include "SnakeBase.h"
#include "SnakeElementsBase.h"



ASnakeBase::ASnakeBase()
{
 	
	PrimaryActorTick.bCanEverTick = true;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	LastMoveDirection = EMovementDirection::UP;

}


void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	AddSnakeElements(4);
	
}

void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	Move(DeltaTime);

}

void ASnakeBase::AddSnakeElements(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		FVector NewLocation(SnakeElements.Num() * ElementSize, 0, 0);
		FTransform NewTransform(NewLocation);
		ASnakeElementsBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementsBase>(SnakeElementsClass, NewTransform);
		NewSnakeElem->AttachToActor(this, FAttachmentTransformRules::KeepRelativeTransform);
		SnakeElements.Add(NewSnakeElem);
	}
}

void ASnakeBase::Move(float Deltatime)
{

	FVector MovementVector;

	float MovementSpeedDelta = MovementSpeed * Deltatime;
	switch (LastMoveDirection)
	{
	case EMovementDirection::UP:
		MovementVector.X += MovementSpeedDelta;
		break;
	case EMovementDirection::DOWN:
		MovementVector.X -= MovementSpeedDelta;
		break;
	case EMovementDirection::LEFT:
		MovementVector.Y += MovementSpeedDelta;
		break;
	case EMovementDirection::RIGHT:
		MovementVector.X -= MovementSpeedDelta;
		break;
	}
	AddActorLocalOffset(MovementVector);
}

